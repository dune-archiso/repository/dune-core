# [`dune-core`](https://gitlab.com/dune-archiso/repository/dune-core) repository for Arch Linux

[![pipeline status](https://gitlab.com/dune-archiso/repository/dune-core/badges/main/pipeline.svg)](https://gitlab.com/dune-archiso/repository/dune-core/-/commits/main)
[![coverage report](https://gitlab.com/dune-archiso/repository/dune-core/badges/main/coverage.svg)](https://gitlab.com/dune-archiso/repository/dune-core/-/commits/main)

This is a third-party auto-updated repository with the following [tarballs](https://gitlab.com/dune-archiso/repository/dune-core/-/raw/main/packages.x86_64). The DUNE core modules are here.

## Usage

### Add repository

1. Import GPG key from GPG servers.

```console
[user@hostname ~]$ sudo pacman-key --recv-keys 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --finger 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --lsign-key 2403871B121BD8BB
```

2. Add the following code to `/etc/pacman.conf`:

```toml
[dune-core]
SigLevel = Required DatabaseOptional
Server = https://dune-archiso.gitlab.io/repository/dune-core/$arch
```

### List packages

To show actual packages list:

```console
[user@hostname ~]$ pacman -Sl dune-core
```

### Install packages

To install package:

```console
[user@hostname ~]$ sudo pacman -Syu
[user@hostname ~]$ sudo pacman -S dune-istl
```

### `PKGBUILD`s from the [Arch User Repository](https://wiki.archlinux.org/title/Arch_User_Repository) 👀

- [`dune-core`](https://aur.archlinux.org/packages/dune-core)
- [`dune-common`](https://aur.archlinux.org/pkgbase/dune-common)
- [`dune-istl`](https://aur.archlinux.org/packages/dune-istl)
- [`dune-geometry`](https://aur.archlinux.org/packages/dune-geometry)
- [`dune-localfunctions`](https://aur.archlinux.org/packages/dune-localfunctions)
- [`dune-grid`](https://aur.archlinux.org/packages/dune-grid)

<!--
- [`dune-common`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-common/README.md)
- [`dune-geometry`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-geometry/README.md)
- [`dune-dune-istl`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-istl/README.md)
- [`dune-localfunctions`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-localfunctions/README.md)
- [`dune-grid`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-grid/README.md)
-->

- [Latest build](https://gitlab.com/dune-archiso/repository/dune-core/-/pipelines/latest)
