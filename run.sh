#!/usr/bin/env bash

if ! [ -e PKGBUILD ]; then
  URL="https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-common/PKGBUILD"
  curl -O $URL
fi

if ! [ -e dune-common-2.9.1 ]; then
  makepkg --allsource
  tar -xvf dune-common-2.9.1.tar.gz
fi
# unset DUNE_PYTHON_WHEELHOUSE
MAKE_DEPENDENCIES="cmake \
doxygen \
graphviz \
python-sphinx \
python-mpi4py \
python-setuptools \
python-portalocker \
python-jinja \
python-numpy \
python-ninja \
python-wheel \
parmetis \
scotch \
"

sudo pacman --needed --noconfirm -S ${MAKE_DEPENDENCIES}
python -m venv --system-site-packages build-cmake/dune-env
cmake \
  -S dune-common-2.9.1 \
  -B build-cmake \
  -DCMAKE_BUILD_TYPE=None \
  -DCMAKE_INSTALL_PREFIX=/usr \
  -DBUILD_SHARED_LIBS=TRUE \
  -DCMAKE_CXX_STANDARD=17 \
  -DCMAKE_C_COMPILER=gcc \
  -DCMAKE_CXX_COMPILER=g++ \
  -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
  -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
  -DCMAKE_VERBOSE_MAKEFILE=ON \
  -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
  -DENABLE_HEADERCHECK=ON \
  -DDUNE_ENABLE_PYTHONBINDINGS=ON \
  -DDUNE_PYTHON_INSTALL_LOCATION='none' \
  -DCMAKE_DISABLE_FIND_PACKAGE_Vc=TRUE \
  -Wno-dev
# export DUNE_PYTHON_WHEELHOUSE=""
# -DDUNE_PYTHON_WHEELHOUSE="/workspace/dune-core/python" \

pushd build-cmake
cmake -LAH >options.txt 2>&1
popd

cmake --build build-cmake --target all
cmake --build build-cmake --target build_tests
ctest --test-dir build-cmake

# makepkg --noconfirm -s 2>&1 | tee -a $(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').log >/dev/null
